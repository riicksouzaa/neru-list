import knex from "knex"
import databaseConfig from "./config"

const db = knex(databaseConfig.sqlite)

export default db