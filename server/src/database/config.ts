import path from "path"
import * as dotenv from "dotenv"
dotenv.config()

const databaseConfig = {
    sqlite: {
        client: 'sqlite3',
        connection: {
            filename: path.resolve(__dirname, 'database.sqlite')
        },
        pool: {
            min: 10,
            max: 24,
            propagateCreateError: false
        },
        migrations: {
            directory: path.resolve(__dirname, 'migrations')
        },
        useNullAsDefault: true
    },
    mongodb: {
        address: process.env.MONGODBSTRING,
        collection: "todo",
        options: {
            useUnifiedTopology: true
        }
    }
}

export default databaseConfig