import express from "express"
import List from "../controllers/list"

const listRoute = express.Router()
const list = new List()

listRoute.get('/', list.getAllLists)
listRoute.post('/', list.create)
listRoute.put('/', list.update)
listRoute.delete('/', list.delete)

export default listRoute