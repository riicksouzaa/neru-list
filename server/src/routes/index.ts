import express from "express"
import listRoute from "./list"

const routes = express.Router()

routes.use('/list', listRoute)

export default routes