import { Request, Response } from "express"

import db from "../database/conn"

export default class List {
    async getAllLists(req: Request, resp: Response) {
        try {
            const list = await db('list').select("*")
            return resp.status(200).json(list)
        } catch (e) {
            console.log(e)
            return resp.status(401).json(e)
        }
    }

    async create(req: Request, resp: Response) {
        const {
            title,
            description
        } = req.body

        const trx = await db.transaction()
        try {
            await trx('list').insert({
                title,
                description
            })
            await trx.commit()
            return resp.status(201).send()
        } catch (e) {
            await trx.rollback()
            return resp.status(400).json({
                error: 'Unexpected error while creating new list'
            })
        }
    }

    async update(req: Request, resp: Response) { 
        let {
            id,
            description,
            status
        } = req.body

        status = (status != null ? status: 0)

        const trx = await db.transaction()
        try{
            await trx('list').where('id' , '=', id).update({description, status})
            await trx.commit()
            return resp.status(201).send()
        }catch(e){
            await trx.rollback()
            return resp.status(400).json({
                error: 'Unable to edit this list'
            })
        }
    }

    async delete(req: Request, resp: Response){
        let {
            id
        } = req.body
        const trx = await db.transaction()
        try{
            await trx('list').where('id' , '=', id).delete()
            await trx.commit()
            return resp.status(200).send()
        }catch(e){
            await trx.rollback()
            return resp.status(400).json({
                error: 'Unable to edit this list'
            })
        }
    }
}