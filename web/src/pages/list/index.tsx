import React, { useState, useEffect, FormEvent } from 'react';
import { Link, useHistory } from 'react-router-dom'
import ListItem, { List } from '../../components/ListItem'
import Input from '../../components/Input'

import api from '../../services/api'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'


function TodoList() {

    const history = useHistory()

    const [list, setList] = useState([])

    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")

    useEffect(() => {
        api.get('list').then(resp => {
            const list = resp.data
            setList(list)
        })
    })

    function createList(e: FormEvent) {
        e.preventDefault()

        api.post('list', {
            title, description
        }).then(() => {
            console.log('success! List added successfuly')
            setTitle("")
            setDescription("")
            history.push('/')
        }).catch(() => {
            console.log("Error in list add.")
        })
    }

    return (
        <div className="container m-5 p-2 rounded mx-auto bg-light shadow">
            <div id="page-title" className="row m-1 p-4">
                <div className="col">
                    <div className="p-1 h1 text-primary text-center mx-auto display-inline-block">
                        <FontAwesomeIcon icon={faCheck} /> <u>To Do</u>
                    </div>
                </div>
            </div>

            <div className="row m-1 p-3">
                <div className="col col-11 mx-auto">
                    <div className="row bg-white rounded shadow-sm p-2 add-todo-wrapper align-items-center justify-content-center">
                        <form onSubmit={createList}>
                            <fieldset>
                                <legend>Insira um item à lista de afazeres com a descrição</legend>
                                <div className="row">
                                    <div className="col">
                                        <Input name="title" label="Título" value={title} placeholder="Insira o título para o item" onChange={(e) => { setTitle(e.target.value) }} />
                                    </div>
                                    <div className="col">
                                        <Input name="description" label="Descrição" placeholder="Insira uma descrição para o item" value={description} onChange={(e) => { setDescription(e.target.value) }} />
                                    </div>
                                </div>
                            </fieldset>
                            <button className="btn btn-block btn-info" type="submit">Cadastrar Item à lista de afazeres</button>
                        </form>
                    </div>
                </div>
            </div>

            <div className="row m-1 p-3">
                <div className="col col-11 mx-auto">


                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Feito</th>
                                <th>Título</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                list.map((list: List) => {
                                    return <ListItem key={list.id} list={list} />
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>


        </div>
    )
}

export default TodoList