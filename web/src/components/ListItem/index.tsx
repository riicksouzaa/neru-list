import React from "react"
import api from "../../services/api"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import Checkbox from '@material-ui/core/Checkbox'
import TextField from '@material-ui/core/TextField'
import Input from '../Input'

export interface List { id: number, title: string, description: string, status: number, created_at: string, updated_at: string }

interface ListItemProps {
    list: List
}

const ListItem: React.FC<ListItemProps> = ({ list }) => {

    let {
        id,
        title,
        description,
        status,
        created_at,
        updated_at
    } = list



    const [checked, setChecked] = React.useState((status === 1 ? true : false));
    const [edit, setEditing] = React.useState(false)

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        status = (event.target.checked ? 1 : 0)
        setChecked(event.target.checked);
        markAsChecked()
    };

    function markAsChecked() {

        api.put(`list`, { id, status }).then(() => {
            console.log("successfuly deleted.")
        }).catch(() => {
            console.log("error on delete.")
        })
    }


    function removeListItem() {
        const id = list.id
        api.delete(`list`, { data: { id } }).then(() => {
            console.log("successfuly deleted.")
        }).catch(() => {
            console.log("error on delete.")
        })
    }

    function editItem() {
        setEditing(!edit) 
    }

    function editDescription(description:string) {
        api.put(`list`, { id, description }).then(() => {
            console.log("successfuly deleted.")
        }).catch(() => {
            console.log("error on delete.")
        })
    }


    return (
        <tr key={list.id}>
            <td><Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }} /></td>
            <td>{list.title}</td>
            <td>{edit ? <TextField fullWidth autoFocus placeholder={list.description} onChange={(e) => { editDescription(e.target.value) }} /> : list.description}</td>
            <td>
                <button className="btn btn-info" onClick={editItem}> <FontAwesomeIcon icon={faPencilAlt} /></button>
                <button className="btn btn-danger" onClick={removeListItem}> <FontAwesomeIcon icon={faTrash} /></button>
            </td>
        </tr>
    )
}

export default ListItem