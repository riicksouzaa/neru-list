import React, { InputHTMLAttributes } from 'react'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    label: string,
    name: string
}

const Input: React.FC<InputProps> = ({ label, name, ...rest }) => {
    return (
        <div className="form-group">
            <label htmlFor={name}>{label}</label>
            <input className="form-control" type="text" id={name} {...rest} />
        </div>
    )
}

export default Input;