import React from 'react'

import { BrowserRouter, Route } from 'react-router-dom'
import List from './pages/list';

function Routes() {
    return (
    <BrowserRouter>
    <Route path="" exact component={List}/>
    </BrowserRouter>
    )
}

export default Routes;