# Tutorial execução Aplicação todolist

#### Considerações
- Foi utilizado o banco de dados sqlite3 devido a sua facilidade e praticidade para a aplicação
- foi escolhido knex para gerencia pois o mesmo traz uma abstração gigante para atuar com databases
- o sistema foi dividido em 2 pastas `web` e `server` sendo essas responsáveis pelo front e pela api respectivamente
- conforme necessidade altere o arquivo `server\src\app.ts` na linha 10 para escolher a porta onde irá executar a api
- caso altere a porta de execução deve-se alterar também o arquivo `web/src/services/api.ts` conforme porta alterada no arquivo `server\src\app.ts`

## Endpoints
1. GET http://localhost/list - Responsável por listar todas as listas
2. POST http://localhost/list Param: JSON [{"title":"tituloExemplo","description":"descricaoExemplo"}] - Responsável por incluir uma nova lista
3. PUT http://localhost/list Param: JSON [{"id":1,"description":"edicaoExemplo","status":1}] - Responsável pela edição de uma lista pelo id
4. DELETE http://localhost/list Param: JSON ["id":1] - Responsável por deletar uma lista pelo id
## Server (API)
Passo a passo para subir a api.
Considerando que o node esteja instalado,
acesse a pasta do projeto va terminal e execute os comandos:

Escolha um dos procedimentos abaixo:
1. NPM
```bash
cd ./server/

npm i
npm run knex:migrate
npm run start
```

2. YARN (necessário yarn instalado)
```bash
cd ./server/

yarn install
yarn knex:migrate
yarn start
```

Deixe a Api em execução, ela estará executando na porta 3333 (localhost)

## Web (Front)

- Escolha um dos procedimentos abaixo:
1. NPM
```bash
cd ./web/

npm i
npm run start
```

2. YARN
```bash
cd ./web/

yarn install
yarn start
```


## Importante

Se ocorrer algum problema ao instalar a lib 
sqlite3 para node version acima de 15
utilize o comando:
```bash
npm install --global --production windows-build-tools
Ou
yarn global add windows-build-tools
```